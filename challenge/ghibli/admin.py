from django.contrib import admin

from ghibli.models import Movie, People


class MovieAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "title",
        "director",
        "producer",
        "release_date",
        "rt_score",
    )


class PeopleAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "gender",
        "age",
        "eye_color",
        "hair_color",
        "url",
        "movies_count",
    )
    filter_horizontal = ["movies"]


admin.site.register(Movie, MovieAdmin)
admin.site.register(People, PeopleAdmin)
