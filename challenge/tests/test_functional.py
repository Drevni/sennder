import time

from django.contrib.auth.models import User
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from selenium.webdriver.firefox.webdriver import WebDriver


class AuthenticationTests(StaticLiveServerTestCase):
    def __init__(self, methodName="runTest"):
        super().__init__(methodName="runTest")
        self.username = "test"
        self.email = "test@test.com"
        self.test_pass = "test_pass"

    @classmethod
    def setUpClass(cls):
        super(AuthenticationTests, cls).setUpClass()
        cls.browser = WebDriver()
        cls.browser.implicitly_wait(25)

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super(AuthenticationTests, cls).tearDownClass()

    def runTest(self):
        user = User.objects.create_superuser(
            self.username, self.email, self.test_pass
        )
        user.save()
        self.browser.get(f"{self.live_server_url}/admin/login/")
        self.browser.find_element_by_name("username").send_keys(self.username)
        self.browser.find_element_by_name("password").send_keys(self.test_pass)
        self.browser.find_element_by_tag_name("input[type='submit']").click()
        time.sleep(5)
        self.assertEqual(
            "Site administration | Django site admin", self.browser.title
        )

        user.delete()
