#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest

from django.test import TestCase

from ghibli.models import Movie, People
from worker import dict_filter, get_headers, movies_injection, people_injection


class WorkerTests(TestCase):
    def setUp(self):
        self.films_api_response = [
            {
                "id": "2baf70d1-42bb-4437-b551-e5fed5a87abe",
                "title": "Castle in the Sky",
                "description": "The orphan Sheeta inherited a mysterious crystal that links her to the mythical sky-kingdom of Laputa. With the help of resourceful Pazu and a rollicking band of sky pirates, she makes her way to the ruins of the once-great civilization. Sheeta and Pazu must outwit the evil Muska, who plans to use Laputa's science to make himself ruler of the world.",
                "director": "Hayao Miyazaki",
                "producer": "Isao Takahata",
                "release_date": "1986",
                "rt_score": "95",
            },
            {
                "id": "12cfb892-aac0-4c5b-94af-521852e46d6a",
                "title": "Grave of the Fireflies",
                "description": "In the latter part of World War II, a boy and his sister, orphaned when their mother is killed in the firebombing of Tokyo, are left to survive on their own in what remains of civilian life in Japan. The plot follows this boy and his sister as they do their best to survive in the Japanese countryside, battling hunger, prejudice, and pride in their own quiet, personal battle.",
                "director": "Isao Takahata",
                "producer": "Toru Hara",
                "release_date": "1988",
                "rt_score": "97",
            },
        ]
        self.people_api_response = [
            {
                "id": "ba924631-068e-4436-b6de-f3283fa848f0",
                "name": "Ashitaka",
                "gender": "male",
                "age": "late teens",
                "eye_color": "brown",
                "hair_color": "brown",
                "films": [
                    "https://ghibliapi.herokuapp.com/films/030555b3-4c92-4fce-93fb-e70c3ae3df8b"
                ],
                "species": "https://ghibliapi.herokuapp.com/species/af3910a6-429f-4c74-9ad5-dfe1c4aa04f2",
                "url": "https://ghibliapi.herokuapp.com/people/ba924631-068e-4436-b6de-f3283fa848f0",
            },
            {
                "id": "030555b3-4c92-4fce-93fb-e70c3ae3df8b",
                "name": "Yakul",
                "age": "Unknown",
                "gender": "male",
                "eye_color": "Grey",
                "hair_color": "Brown",
                "films": [
                    "https://ghibliapi.herokuapp.com/films/0440483e-ca0e-4120-8c50-4c8cd9b965d6"
                ],
                "species": "https://ghibliapi.herokuapp.com/species/6bc92fdd-b0f4-4286-ad71-1f99fb4a0d1e",
                "url": "https://ghibliapi.herokuapp.com/people/030555b3-4c92-4fce-93fb-e70c3ae3df8b",
            },
        ]

    def test_movie_creation(self):
        movies_injection(self.films_api_response)
        self.assertEqual(Movie.objects.count(), 2)

    def test_people_creation(self):
        people_injection(self.people_api_response)
        self.assertEqual(People.objects.count(), 2)

    def test_get_headers(self):
        self.assertTrue(isinstance(get_headers(), dict))

    def test_dict_filter_checker(self):
        _test_dict = {"1": 1, "2": 2, "3": 3, "4": 4}
        _keys = ["2", "4"]
        new_dict = dict_filter(_test_dict, _keys)
        self.assertEqual(len(new_dict.keys()), 2)

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main(failfast=True, verbosity=2)
