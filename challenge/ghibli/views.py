# -*- coding: utf-8 -*-
from django.views.generic import ListView

from ghibli.models import Movie


class MoviesList(ListView):
    model = Movie
