import datetime
import uuid

from django.db import models
from django.db.models import QuerySet


class Movie(models.Model):
    """
    Movies data
    """

    id = models.UUIDField(
        verbose_name="Movie ID", primary_key=True, editable=False
    )

    title = models.CharField(
        verbose_name="Title of movie", max_length=225, blank=True, null=True
    )
    description = models.TextField(
        verbose_name="Description of the movie", blank=True, null=True
    )
    director = models.CharField(
        verbose_name="Director", max_length=128, blank=True, null=True
    )
    producer = models.CharField(
        verbose_name="Producer", max_length=128, blank=True, null=True
    )
    release_date = models.PositiveSmallIntegerField(
        verbose_name="Release Date", default=0
    )
    rt_score = models.PositiveSmallIntegerField(
        default=0, verbose_name="Score"
    )

    class Meta:
        verbose_name = "Movie"
        verbose_name_plural = "Movies"

    def __str__(self):
        return self.title

    def people(self) -> QuerySet:
        return People.objects.filter(movies=self)

    def people_text(self) -> str:
        return ", ".join(self.people().values_list("name", flat=True))


class People(models.Model):
    """
    Creators of videos
    """

    GENDER_MALE = "m"
    GENDER_FEMALE = "f"
    GENDER_NONE = "n"
    GENDER = [
        (GENDER_MALE, "Male"),
        (GENDER_FEMALE, "Female"),
        (GENDER_NONE, "no data"),
    ]
    id = models.UUIDField(
        verbose_name="Person ID", primary_key=True, editable=False
    )

    name = models.CharField(
        verbose_name="Name of creator", max_length=225, blank=True, null=True
    )
    gender = models.CharField(
        verbose_name="Gender",
        choices=GENDER,
        max_length=10,
        blank=True,
        null=True,
    )
    age = models.CharField(verbose_name="Age", max_length=32)
    eye_color = models.CharField(
        verbose_name="Eye color", max_length=32, blank=True, null=True
    )
    hair_color = models.CharField(
        verbose_name="Hair color", max_length=32, blank=True, null=True
    )
    url = models.URLField(verbose_name="Profile url", blank=True, null=True)
    movies = models.ManyToManyField(Movie)

    class Meta:
        verbose_name = "Person"
        verbose_name_plural = "People"

    def __str__(self):
        return self.name

    def movies_count(self) -> int:
        return self.movies.count()
