from django.urls import path
from django.views.generic.base import RedirectView

from .views import MoviesList

urlpatterns = [
    path("", RedirectView.as_view(url="/movies"), name="homepage"),
    path("movies/", MoviesList.as_view(), name="movies"),
]
