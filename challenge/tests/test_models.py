import unittest
from random import randint

from django.contrib.auth.models import User
from django.test import TestCase

from ghibli.models import Movie, People


class MovieModelTest(TestCase):
    def test_string_representation(self):
        obj = Movie(title="Nice title")
        self.assertEqual(str(obj), obj.title)

    def test_verbose_name_plural(self):
        self.assertEqual(str(Movie._meta.verbose_name_plural), "Movies")

    def create_movie(self):
        ident = int("".join([str(randint(0, 9)) for x in range(10)]))
        return Movie.objects.create(id=ident, title="Test Title")

    def test_movie_creation(self):
        obj = self.create_movie()
        self.assertTrue(isinstance(obj, Movie))
        self.assertEqual(obj.__str__(), obj.title)


class UserModelTest(TestCase):
    def test_user_create(self):
        User.objects.create_user("test", "test@test.com", "testname")
        self.assertEqual(User.objects.count(), 1)


if __name__ == "__main__":
    unittest.main()
