## Sennder Challenge ##

#### RUN STEPS
#### Install poetry package:
```
sudo pip install poetry
```

Go to project directory
```
cd challenge
```
#### Install all required python packages
```
poetry config settings.virtualenvs.in-project false
poetry install
```

#### Run django server
```
make run
```
#### The second part of application is worker.
The worker is responsible for get data from ghibli's API at minute intervals. Worker can be run as daemon process as well.
To run it please type:
```
make worker
```
The worker code you can find in worker.py file


### The tests you can run by:
```
make test
```
or
```
make pytest
```


### RUN application using docker containers
From main directory:
```
make docker-build
```
After build will be created few containers. To run:
```
make docker-up
```
