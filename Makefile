SHELL := /bin/bash

.PHONY: help

help:   ## print the help message
		@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

clean:  ## clean the project form temp files
		@-find . -name "__pycache__" -exec rm -rf {} \; 2>/dev/null || true
		@-find . -name "*.pyc" -exec rm -rf {} \; 2>/dev/null || true
		@-find . -name ".pytest_cache" -exec rm -rf {} \; 2>/dev/null || true
		@-find ../../ -name "pip-wheel-metadata" -exec rm -rf "{}" \; 2>/dev/null || true

docker-migrate:	## docker migration executor
		source .env
		@-docker-compose exec web python manage.py migrate --noinput

docker-build:	## docker build images
		source .env
		@-docker-compose build

docker-up:	## docker build images
		source .env
		@-docker-compose up

tester:	##	remove ewerything from docker resources !!! not use if you dont know what it does
		@echo -n "Are you sure? [y/N] " && read ans && [ $${ans:-N} = y ]

docker-total-cleaner:	##	remove ewerything from docker resources !!! not use if you dont know what it does
		docker stop `docker ps -a -q` ; 2>/dev/null || true
		docker rm `docker ps -a -q`; 2>/dev/null || true
		docker system prune -a
		docker images purge
		docker volume prune
		# docker network rm `docker network ls | grep "bridge" | awk '/ / { print $1 }'`
