import unittest

from django.test import TestCase
from django.urls import reverse


class MoviesPageTests(TestCase):
    def test_movies_page_status_code(self):
        response = self.client.get("/movies", {}, True)
        self.assertEqual(response.status_code, 200)

    def test_view_url_by_name(self):
        response = self.client.get(reverse("movies"), {}, True)
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse("movies"), {}, True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "ghibli/movie_list.html")

    def test_movies_page_contains_correct_html(self):
        response = self.client.get("/movies", {}, True)
        self.assertContains(response, "<h4>The list of Ghibli movies</h4>")

    def test_movies_page_does_not_contain_incorrect_html(self):
        response = self.client.get("/movies", {}, True)
        self.assertNotContains(response, "footer")
        self.assertContains(response, "Challenge Sennder Task")

    def test_homepage_redirection(self):
        """ check redirection """
        response = self.client.get("/")
        self.assertEqual(response.status_code, 302)


class adminPageTests(TestCase):
    def test_admin_page_status_code(self):
        response = self.client.get("/admin", {}, True)
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
