import asyncio
import datetime
import logging
import os
import time
from typing import Dict, List

import django
import requests
from user_agent import generate_user_agent

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "challenge.settings")
django.setup()

TIMER = 60
GHIBLI_BASE_URL = "https://ghibliapi.herokuapp.com"

logging.basicConfig(level=logging.DEBUG)


def get_headers() -> str:
    return {
        "Content-Type": "application/json",
        "User-Agent": generate_user_agent(),
    }


def dict_filter(dict_obj: Dict, keys: List) -> Dict:
    return {k: v for (k, v) in dict_obj.items() if k in keys}


def movies_injection(movies: List) -> None:
    """ Add movies to DB """
    from ghibli.models import Movie, People

    for movie in movies:
        data = dict_filter(
            movie,
            [
                "title",
                "descrition",
                "director",
                "producer",
                "release_date",
                "rt_score",
            ],
        )
        obj, created = Movie.objects.update_or_create(
            id=movie["id"], defaults=data
        )


def people_injection(people: List) -> None:
    """ Add People to DB """
    from ghibli.models import Movie, People

    for person in people:
        data = dict_filter(
            person, ["name", "gender", "age", "eye_color", "hair_color", "url"]
        )
        obj, created = People.objects.update_or_create(
            id=person["id"], defaults=data
        )
        for film in person["films"]:
            _ident = film.split("/")[-1]
            obj_movie = Movie.objects.filter(id=_ident).first()
            if obj_movie:
                obj.movies.add(obj_movie)
            else:
                logging.info(f"Movie is not in DB #TODO {_ident}")
                # TODO make api request to get details of movie,
                # who is not in database and insert to DB as well


# TODO Sugestion in future - use async method with async_to_sync method
def periodic() -> None:
    """ infinity loop, Regular get data from API """
    uri_movies = f"{GHIBLI_BASE_URL}/films?limit=250"
    uri_people = f"{GHIBLI_BASE_URL}/people?limit=250"

    while True:
        logging.info(f"Periodic started at {datetime.datetime.now()}")
        with requests.Session() as session:
            session.headers.update(get_headers())
            movies = session.get(uri_movies).json()
            movies_injection(movies)
            people = session.get(uri_people).json()
            people_injection(people)
        logging.info(f"Periodic end")
        time.sleep(TIMER)


if __name__ == "__main__":
    logging.info(f"Start worker {datetime.datetime.now()}")
    loop = asyncio.get_event_loop()
    loop.create_task(periodic())

    try:
        loop.run_forever()
    except asyncio.CancelledError:
        pass
    finally:
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()
