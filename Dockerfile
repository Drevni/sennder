FROM python:3.8-slim-buster

ARG PACKAGE_NAME=challenge
ARG PACKAGE_PATH=/code/$PACKAGE_NAME

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PATH=${PATH}:/code/$PACKAGE_NAME
ENV PYTHONPATH=$PACKAGE_PATH
ENV LIBRARY_PATH=/lib:/usr/lib

RUN mkdir /code
WORKDIR /code

RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates python3-dev python3-cachetools gcc netcat \
    build-essential libssl-dev libffi-dev python-dev python3-pyparsing \
    && rm -rf /var/lib/apt/lists/*

COPY docker-entrypoint.sh /code/
RUN chmod 770 /code/docker-entrypoint.sh
COPY $PACKAGE_NAME /code/
# COPY pyproject.toml /code/

RUN pip install --upgrade pip
RUN pip install poetry
RUN pip install pyparsing
RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction --no-dev

# ENTRYPOINT ["python"]
ENTRYPOINT ["./docker-entrypoint.sh"]
