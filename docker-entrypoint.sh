#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

# python manage.py flush --no-input
python manage.py migrate

exec "$@"

gunicorn --bind 0.0.0.0:8000 challenge.wsgi:application --name challenge --timeout 2900 --workers 7 --daemon
python worker.py
